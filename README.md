## audios

This repository contains the source code for `audios`.

The purpose of `audios` is to control the active audio output device without the GUI.

### Usage

```bash
# audios uses switchaudio-osx under the hood
brew install switchaudio-osx

audios
```
