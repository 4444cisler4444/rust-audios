use csv;
use serde::{Deserialize, Serialize};
use std::process::Command;

#[derive(Serialize, Deserialize, Debug)]
struct AudioDevice {
    name: String,
    /// Either "input" or "output"
    device_type: String,
    /// May look like "60-ab-d2-34-17-f4"
    id: String,
    extra: String,
}

// List audio devices 0..N (selectable)
// Get input char 0..N, this is the selected audio device
// Display actions for that audio device
fn main() {
    // List current output device
    let active_output = Command::new("SwitchAudioSource")
        .arg("-f")
        .arg("cli")
        .arg("-c")
        .stdout(std::process::Stdio::piped())
        .output()
        .expect("Dependency not installed");

    if !active_output.status.success() {
        println!("{}", String::from_utf8_lossy(&active_output.stderr));
        std::process::exit(active_output.status.code().unwrap_or(1));
    }

    let list = Command::new("SwitchAudioSource")
        .arg("-f")
        .arg("cli")
        .arg("-a")
        .stdout(std::process::Stdio::piped())
        .output()
        .expect("Failed to get audio device list. Run brew install switchaudio-osx");

    let str = String::from_utf8_lossy(&list.stdout);

    let mut audio_devices = Vec::new();
    let mut rdr = csv::Reader::from_reader(str.as_bytes());
    for result in rdr.records() {
        let record = result.unwrap();
        let name = &record[0];
        let device_type = record[1].parse::<String>().ok().unwrap();
        let id: String = record[2].parse().unwrap();
        let extra: String = record[3].parse().unwrap();
        audio_devices.push(AudioDevice {
            name: name.to_string(),
            device_type,
            id,
            extra,
        });
    }

    // filter out input devices
    audio_devices = audio_devices
        .into_iter()
        .filter(|d| match d.device_type.as_str() {
            "input" => false,
            "output" => true,
            _ => unimplemented!("Unknown device type"),
        })
        .collect();

    println!("Sound Output Devices");

    // print selectable list of audio_devices
    for i in 0..audio_devices.len() {
        print!("{})", i + 1);
        println!(" {:?}", audio_devices[i].name);
    }

    // println!(
    //     "Select output device (between 1 and {})",
    //     audio_devices.len()
    // );

    // get selected audio device
    let selected_index = get_selected_number(audio_devices.len() + 1);

    // select audio device
    Command::new("SwitchAudioSource")
        .arg("-t")
        .arg("output")
        .arg("-s")
        .arg(&audio_devices[selected_index - 1].name)
        .stdout(std::process::Stdio::piped())
        .output()
        .expect("Failed to set the audio device by name.");
    println!(
        "Selected Audio Device: {:?}",
        audio_devices[selected_index - 1].name
    );
}

fn get_selected_number(max: usize) -> usize {
    let mut input_char = String::new();
    // Read input
    std::io::stdin().read_line(&mut input_char).unwrap();
    // Trim whitespace and convert to usize
    let mut res = input_char.trim().parse::<usize>().unwrap();
    // Ensure input is in range
    while res > max {
        println!("Select output device (between 1 and {})", max - 1);
        res = get_selected_number(max);
    }
    res
}
